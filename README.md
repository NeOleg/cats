# Cats
Get Started
If you have not yet installed React Native, you can use this tutorial.

Use git clone to get project. Then go to the root folder of project and install all node modules using npm install command.

Run on Android
    You have to connect hardware device using ADB or run emulator.
    Invoke react-native run-android command.
Run on iOS
    You have to get Xcode installed on your machine.
    Invoke react-native run-ios command.