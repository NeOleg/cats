import { Dimensions, StyleSheet } from "react-native";

export const catsStyle = StyleSheet.create({
    itemContainer: { 
        width: Dimensions.get('screen').width - 10, 
        height: 230,
        marginTop: 60,
        marginHorizontal: 5,
        justifyContent: 'center',
    },
    image: {
        height: 200,
        width: '100%',
        borderRadius: 10,
    },
    textStyle: {
        fontSize: 20,
        justifyContent: 'flex-start',
        marginTop: 10,
    },
    descriptionStyle: {
        fontSize: 16,
        justifyContent: 'flex-start',
        marginTop: 5,
    },
})