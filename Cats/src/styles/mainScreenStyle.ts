import { Dimensions, StyleSheet } from "react-native";

export const mainScreenStyle = StyleSheet.create({
    flatListStyle: {
        width: '100%',
        height: Dimensions.get('screen').height-230,
    },
    activityStyle: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemContainer: {
        width: Dimensions.get('screen').width - 10,
        height: 230,
        margin: 5,
        borderColor: 'blue',
        borderRadius: 10,
        borderWidth: 1,
    },
    image: {
        height: 200,
        width: '100%',
        borderRadius: 10,
    },
    textStyle: {
        fontSize: 20,
        justifyContent: 'center',
        textAlign: 'center',
    },
    buttonContainer: {
        width: Dimensions.get('screen').width - 10,
        height: 70,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: 5
    },
    buttonStyle: {
        width: '30%',
        height: 50,
        borderRadius: 10,
        backgroundColor: 'blue',
        justifyContent: 'center',
    },
    buttonTextStyle: {
        color: 'white',
        fontSize: 25,
        alignSelf: 'center',
        
    },
    connectContainer: {
        width: '100%',
        height: 30,
        justifyContent: 'center',
        backgroundColor: 'red',
        alignSelf: 'center'
    },
    connectText: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        fontWeight: '700'
    },
})