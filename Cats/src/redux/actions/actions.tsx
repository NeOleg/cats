import AsyncStorage from "@react-native-community/async-storage"
import { DOWNLOAD } from "../constans/constantsRedux"



export const downloadData = () => {
    
    const dataFromStor = async () => {
        const res = await AsyncStorage.getItem('data');
        if (res) {
            return JSON.parse(res);
        } 
    }
    const data = dataFromStor();
    return {
        type: DOWNLOAD,
        payload: data
    }
}
