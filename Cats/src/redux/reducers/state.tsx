import { createStore, combineReducers } from 'redux';
import {downloadReducer} from './reducer';

const rootReducer = combineReducers(
    { data: downloadReducer }
);
const configureStore = () => {
    return createStore(rootReducer);
}
export default configureStore;