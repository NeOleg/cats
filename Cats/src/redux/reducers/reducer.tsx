import { DOWNLOAD } from "../constans/constantsRedux";

const initialState = {data: []};
export const downloadReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case DOWNLOAD:
            return {
                ...state,
                data: action.payload
            };
        default:
            return state;
    }
}