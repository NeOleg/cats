import React, { useEffect, useState } from "react";
import { Text, TouchableOpacity, View, FlatList, ActivityIndicator, Image, Touchable } from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import { downloadData } from "../redux/actions/actions";
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob';
import { names, description, noInternet } from '../../src/constants/const'
import { mainScreenStyle } from "../styles/mainScreenStyle";
import NetInfo, { NetInfoState } from "@react-native-community/netinfo";

interface IItem {
    id: number,
    name: string,
    image: any,
    description: string,
}

export const MainScreen = (props: any) => {
    const [DataData, setDataData] = useState<IItem[]>([]);
    const [loadData, setLoadData] = useState(true);
    const [listData, setListData] = useState<IItem[]>([]);
    const [numOfItems, setNumOfItems] = useState(50);
    const [isConnect, setIsConnect] = useState(true);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(downloadData());
    }, [dispatch]);

    useEffect(() => {
        NetInfo.addEventListener(handleInternetConnectionChange);
    }, [isConnect]);

    const handleInternetConnectionChange = (state: NetInfoState) => {
        if (!state.isConnected) {
            setIsConnect(false);
        } else {
            setIsConnect(true);
        }
    };

    const getData = (i: number) => {
        try {
            RNFetchBlob.config({ fileCache: true })
                .fetch(
                    'GET',
                    `http://placekitten.com/${250 + Math.floor(Math.random() * 100)}/${250 + Math.floor(Math.random() * 100)
                    }`,
                )
                .then(res => {
                    return res.readFile('base64');
                })
                .then(base =>
                    setDataData(prevState => [
                        ...prevState,
                        {
                            id: i,
                            image: base,
                            name: names[Math.floor(Math.random() * names.length)],
                            description: description[Math.floor(Math.random() * description.length)],
                        },
                    ]),
                )
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        for (let i = 0; i < 100; i++) {
            getData(i);
        }
    }, []);

    const save = async () => {
        try {
            await AsyncStorage.setItem('data', JSON.stringify(DataData));
        } catch (err) {
            console.log(err);
        }

    }

    const da = useSelector(state => state.data.data);

    useEffect(() => {
        if (DataData.length === 100) {
            save().then(() => setLoadData(false));
            if (da._W) {
                const res = da._W.filter((item: IItem) => item.id < numOfItems)
                setListData(res)
            }
        }
    }, [DataData, numOfItems]);


    const renderItem = (item: any) => {
        return (
            <TouchableOpacity style={mainScreenStyle.itemContainer} onPress={() => props.navigation.navigate('CatScreen', { item })}>
                <Image
                    source={{ uri: `data:image/png;base64,${item.item.image}` }}
                    style={mainScreenStyle.image}
                    resizeMode={'cover'}
                />
                <Text style={mainScreenStyle.textStyle}>{item.item.name}</Text>
            </TouchableOpacity>
        )
    }

    const refreshData = (i: number) => {
        setNumOfItems(i);
        const res = da._W.filter((item: IItem) => item.id < i)
        setListData(res)
    }

    return (
        <View>
            <View style={mainScreenStyle.buttonContainer}>
                <TouchableOpacity style={mainScreenStyle.buttonStyle} onPress={() => refreshData(30)}>
                    <Text style={mainScreenStyle.buttonTextStyle}>30</Text>
                </TouchableOpacity>
                <TouchableOpacity style={mainScreenStyle.buttonStyle} onPress={() => refreshData(50)}>
                    <Text style={mainScreenStyle.buttonTextStyle}>50</Text>
                </TouchableOpacity>
                <TouchableOpacity style={mainScreenStyle.buttonStyle} onPress={() => refreshData(100)}>
                    <Text style={mainScreenStyle.buttonTextStyle}>100</Text>
                </TouchableOpacity>
            </View>
            {!isConnect &&
                <View style={mainScreenStyle.connectContainer}>
                    <Text style={mainScreenStyle.connectText}>{noInternet}</Text>
                </View>
            }
            {loadData ?
                <View style={mainScreenStyle.activityStyle}>
                    <ActivityIndicator size={'large'} />
                </View>
                :
                <FlatList
                    data={listData}
                    renderItem={renderItem}
                    style={mainScreenStyle.flatListStyle}
                    extraData={listData}
                />
            }
        </View>
    );
}
