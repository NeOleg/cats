import React from "react";
import { Image, Text, View } from 'react-native';
import { catsStyle } from "../styles/catsStyle";

export const CatScreen = ({ route }: any) => {
    const item = route.params.item.item;
    return (
        <View style={catsStyle.itemContainer}>
            <Image
                source={{ uri: `data:image/png;base64,${item.image}` }}
                style={catsStyle.image}
                resizeMode={'cover'}
            />
            <Text style={catsStyle.textStyle}>{item.name}</Text>
            <Text style={catsStyle.descriptionStyle}>{item.description}</Text>
        </View>
    );
}