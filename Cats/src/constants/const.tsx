export const names = [
    'Keks',
    'Lolly',
    'Bella',
    'Charlie',
    'Leo',
    'Nila',
    'Leo',
    'Kiti',
    'Dilon',
    'Boby',
  ];

  export const description = [
    'Your cat’s second year is roughly equal to the first 25 of a human. After that, your cat tends to develop more slowly and ages about four to five human years every 12 months.',
    'While humans have six muscles in both of their outer ears, cats have 32 muscles in each of theirs! These muscles give cats the ability to swivel and rotate their ears to pinpoint the exact source of a noise.',
    'Cat hearing is extremely sensitive and can hear tones at much higher pitches than humans. This gives them an advantage in nature, as most of their prey, such as rodents or birds, make high-pitched sounds.',
    'Some of these large domestic cats include the Siberian Cat, Ragdoll, Maine Coon and British Shorthair. These big cats are typically known for their fluffy coats and affectionate nature.',
    'Cats are evolved to sleep most of the time they don’t spend hunting, saving up their strength to catch their dinner. While they are not hunting at home, these evolutionary traits have carried over. Adult cats can sleep from 16 to 20 hours a day. Kittens and older cats can sleep almost 24 hours a day.',
    'While cats are typically known for their powerful vision, they have a blind spot! This is because prey usually does not come directly under a cat’s nose. When prey does come close to the cat, they are much more likely to rely on their sense of smell than their vision.',
    'Adult cats that do not live with humans have clear communication with one another. Cats communicate with one another through scent, facial expression, complex body language and touch. Domesticated cats meow at humans to communicate their needs. To learn more, read our article about why cats meow.',
    'In many countries, myths and fables, white cats are seen as a symbol of good luck and are believed to represent purity and happiness. ',
    'While humans have six muscles in both of their outer ears, cats have 32 muscles in each of theirs! These muscles give cats the ability to swivel and rotate their ears to pinpoint the exact source of a noise.',
    'Cats have five toes on each front paw, but only four on the back ones.',
  ];

  export const noInternet = 'NO INTERNET CONNECTION';