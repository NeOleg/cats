import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { MainScreen } from '../screens/mainScreen';
import { CatScreen } from '../screens/catScreen';


const Stack = createStackNavigator();


export const Navigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name={'MainScreen'} component={MainScreen}  options={{title: 'Main Screen'}}/>
                <Stack.Screen name={'CatScreen'} component={CatScreen} options={{title: 'Cat Screen'}}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}