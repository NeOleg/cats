import React from 'react';
import { Navigation } from './src/navigation/navigation';
import { Provider } from 'react-redux';
import configureStore from './src/redux/reducers/state';

const store = configureStore()
const App = () => {
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
};
export default App;
